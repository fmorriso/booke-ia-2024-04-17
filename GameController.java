import java.util.ArrayList;
public class GameController
{
    //This class creates instances of the other classes and uses them to call the methods for the game
    private Round round;
    private Turn turn;
    private Position pos;
    private Choose choose;
    public GameController()
    {
        this.round = new Round(this);
        this.turn = new Turn(this);
        this.pos = new Position(this);
        this.choose = new Choose(this);
    }
    public void startGame()
    {
        this.round.startGame();
    }

    public int getPos(int n) {
        return this.pos.getPos(n);
    }

    public String getAssignment(int p) {
        return this.pos.getAssignment(p);
    }

    public void incScore() {
        this.round.incScore();
    }

    public void incMatches() {
        this.round.incMatches();
    }

    public int getMatches() {
        return round.getMatches();
    }

    public void endRound() {
        this.round.endRound();
    }

    public void decScore() {
        this.round.decScore();
    }

    public void resetAllCards() {
        this.pos.resetAllCards();
    }

    public void startPos() {
        this.pos.startPos();
    }

    public void setPos() {
        this.pos.setPos();
    }

    public void askForChoice() {
        this.choose.askForChoice();
    }

    public void checkMatch(int a, int b) {
        this.turn.checkMatch(a,b);
    }

    public int getChoice(int i) {
        return this.turn.getChoice(i);
    }

    public void setHighScore() {
        this.round.setHighScore();
    }

    public int getHighScore() {
        return this.round.getHighScore();
    }

    public void resetChoices() {
        this.turn.resetChoices();
    }
    public void addChoice(int i) {
        this.turn.addChoice(i);
    }
    public int getScore() {
        return this.round.getScore();
    }
    public void resetMatches() {
        this.round.resetMatches();
    }
    public void addToMatches(int i) {
        this.round.addToMatches(i);
    }
    public ArrayList<Integer> printMatches() {
        return this.round.printMatches();
    }
    public ArrayList<Integer> getChoices() {
        return this.turn.getChoices();
    }
    public boolean alreadyMatched(int a) {
        return this.round.alreadyMatched(a);
    }
    public int getNewResponse() {
        return this.choose.getNewResponse();
    }
    public int getResponse() {
        return this.choose.getResponse();
    }
    public int getNewChoice() {
        return this.choose.getNewChoice();
    }


}

