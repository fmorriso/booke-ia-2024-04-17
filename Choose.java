import java.util.*;
class Choose {
    private GameController controller;
    public Choose(GameController controller)
    {
        this.controller = controller;
    }

    //Prompts other methods to ask for user input, determine whether it is valid, then record the choices
    public void askForChoice()
    {
        boolean alreadySelected = false;
        System.out.println("Current matches include cards: " + controller.printMatches());
        int a = controller.getResponse();
        while (a < 1 || a > 16)
        {
            System.out.println("That is not a card.");
            a = controller.getResponse();
        }
        alreadySelected = controller.alreadyMatched(a);
        while (alreadySelected == true)
        {
            a = controller.getNewResponse();
            while (a < 1 || a > 16)
            {
                System.out.println("That is not a card.");
                a = controller.getResponse();
            }
            alreadySelected = controller.alreadyMatched(a);
        }
        System.out.println("You chose: " + controller.getAssignment(a));
        controller.addChoice(a);
        int b = controller.getResponse();
        while (b == a)
        {
            b = controller.getNewChoice();
        }
        while (b < 1 || b > 16)
        {
            System.out.println("That is not a card.");
            b = controller.getResponse();
        }
        alreadySelected = controller.alreadyMatched(b);
        while (alreadySelected == true)
        {
            b = controller.getNewResponse();
            while (b < 1 || b > 16)
            {
                System.out.println("That is not a card.");
                b = controller.getResponse();
            }
            alreadySelected = controller.alreadyMatched(b);
        }
        System.out.println("You chose: " + controller.getAssignment(b));
        controller.addChoice(b);

    }

    //Method prompts the user to input chosen card numbers
    public int getResponse()
    {
        System.out.println("Choose a card from 1-16: ");
        try{
            Scanner kb = new Scanner(System.in);
            int response = kb.nextInt();
            return response;
        } catch (Exception e) 
        {
            System.out.println("Please enter a number 1-16: ");
            Scanner kb = new Scanner(System.in);
            int response = kb.nextInt();
            return response;
        } 
    }

    //Method for getting a different number if the one entered by the user has already been matched
    public int getNewResponse()
    {
        System.out.println("You have found the match for that card. Please enter a new number 1-16: ");
        try{
            Scanner kb = new Scanner(System.in);
            int response = kb.nextInt();
            return response;
        } catch (Exception e) 
        {
            System.out.println("Please enter a number 1-16 that you have not previously matched: ");
            Scanner kb = new Scanner(System.in);
            int response = kb.nextInt();
            return response;
        } 
    }

    public int getNewChoice()
    {
       System.out.println("You already chose that card this turn. Please enter a new number 1-16: ");
        try{
            Scanner kb = new Scanner(System.in);
            int response = kb.nextInt();
            return response;
        } catch (Exception e) 
        {
            System.out.println("Please enter a new number 1-16: ");
            Scanner kb = new Scanner(System.in);
            int response = kb.nextInt();
            return response;
        }  
    }
    
}
