import java.util.*;

public class Position
{
    private ArrayList<Integer> positions = new ArrayList<Integer>();
    private final static String[] images = {"wolf","turtle","rabbit","dolphin","parrot","penguin","cheetah","frog"};
    private ArrayList<String> cards = new ArrayList<String>(Arrays.asList(images));
    private ArrayList<String> assignments = new ArrayList<String>();
    private GameController controller;

    public Position(GameController controller)
    {
        this.controller = controller;
        positions = new ArrayList<Integer>();
    }

        //Methods to randomize the positions of the cards and access their values
        public String getAssignment(int a) {
        String obj = assignments.get(a-1);
        return obj;
    }
    public void startPos()
    {
        for (int i = 1; i <= 16; i++)
        {
            positions.add(i);
        }
    }
    public int getPos(int p)
    {
        return positions.get(p);
    }
    public void setPos()
    {
        if (assignments == null || assignments.size() == 0) {
            assignments = new ArrayList<String>();
            for (int i = 0; i < 16; i++) {
                assignments.add(String.format("Assignment %d", i));
            }
        }

        int n = 0;
        for (String c : cards)
        {
            for (int p = 0; p < 2; p++)
            {
                n = (int) (Math.random() * positions.size());
                assignments.set(controller.getPos(n) - 1,c); 
                positions.remove(n);
            }
        }
    }

    //Randomizes card values for a new round
    public void resetAllCards()
    {
          this.startPos();
          this.setPos();  
    }
    
}