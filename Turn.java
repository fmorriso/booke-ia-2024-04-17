import java.util.*;

public class Turn 
{
    private  ArrayList<Integer> choices;
    private GameController controller;

    public Turn(GameController controller)
    {
        choices = new ArrayList<Integer>();
        this.controller = controller;
    }

    public void addChoice(int choice)
    {
        choices.add(choice);
        if (choices.size() == 2)
        {
            int c1 = controller.getChoice(0);
            int c2 = controller.getChoice(1);
            controller.checkMatch(c1, c2);
        }
    }

    //Tracks, clears, and returns the values that the user inputs 
    public ArrayList<Integer> getChoices()
    {
        return choices;
    }
    public void resetChoices()
    {
        choices.clear();
    }
    public int getChoice(int c)
    {
        int choice = choices.get(c);
        return choice;
    }
        
    //Executes different methods depending on whether the user's choices are matching cards or not
    public void checkMatch(int pos1, int pos2)
    {
        if (controller.getAssignment(pos1) == controller.getAssignment(pos2)) {
            controller.incScore();
            controller.addToMatches(pos1);
            controller.addToMatches(pos2);
            controller.resetChoices();
            controller.incMatches();
            if (controller.getMatches() > 7) {
                controller.endRound();
            }
            else{
                System.out.println("Current Score: " + controller.getScore());
            }
        } else {
            controller.decScore();
            controller.resetChoices(); 
            System.out.println("Current Score: " + controller.getScore());
        }
        controller.askForChoice();
    }

}
