import java.util.ArrayList;

public class Round
{
    private GameController controller;
    private int matches;
    private int cScore;
    private int hScore;
    private ArrayList<Integer> matched;

    //When the game starts, set the instance variables
    public Round(GameController controller)
    {
        this.controller = controller;
        this.matches = 0;
        this.cScore = 1000;
        this.matched = new ArrayList<Integer>();
    }
    //Methods to edit and access the scores for a particular round of the game and the overall high score
    public void incScore()
    {
        this.cScore += 100;
    }
    public void decScore()
    {
        this.cScore -= 20;
    }
    public int getScore()
    {
        return this.cScore;
    }
    public int getHighScore()
    {
        return this.hScore;
    }
    public void setHighScore()
    {
        if (this.cScore > this.hScore)
        {
            this.hScore = this.cScore;
            System.out.println("You got a new High Score!: " + controller.getHighScore());
        }
        else{
            System.out.println("Current High Score: " + controller.getHighScore());
        }
    }

    //Methods to track the number of matches found in a given round
    public void incMatches()
    {
        matches++;
        if(this.matches == 8)
        {
            controller.endRound();
        }
    }
    public int getMatches()
    {
        return this.matches;
    }

    //Method to start the first round when the program is initialized
    public void startGame()
    {
        System.out.println("Welcome to the matching game!");
        controller.startPos();
        controller.setPos();
        controller.askForChoice();
    }

    //Ends the round, resets variable values, and outputs a high score based on current round and previous ones
    public void endRound()
    {
        controller.resetAllCards();
        this.matches = 0;
        controller.setHighScore();
        this.cScore = 1000;
        controller.resetMatches();
        controller.askForChoice();
    }

    //Methods to track which cards have been matched and ensure that cards that have already been matched cannot be selected again
    public void addToMatches(int m)
    {
        this.matched.add(m);
    }
    public void resetMatches()
    {
        this.matched = new ArrayList<Integer>();
    }
    public boolean alreadyMatched(int a)
    {
        for (Integer m : controller.printMatches())
        {
            if(a == m)
            {
                return true;
            }
        }
        return false;
    }
    public ArrayList<Integer> printMatches()
    {
        return this.matched;
    }

}
